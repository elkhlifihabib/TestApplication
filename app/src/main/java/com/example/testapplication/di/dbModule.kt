package com.example.testapplication.di

import androidx.room.Room
import com.example.testapplication.data.db.AppDb
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val dbModule = module {
    single {
        Room.databaseBuilder(androidApplication(), AppDb::class.java, "app.db")
            .build()
    }
}