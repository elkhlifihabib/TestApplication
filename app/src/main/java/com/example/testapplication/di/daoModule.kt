package com.example.testapplication.di

import com.example.testapplication.data.db.AppDb
import org.koin.dsl.module

val daoModule = module {
    factory { get<AppDb>().albumDao() }
}