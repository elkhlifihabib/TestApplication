package com.example.testapplication.data.db.album

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "album")
data class Album(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var albumId: Long,
    var title: String?,
    var url: String?,
    var thumbnailUrl: String?
)