package com.example.testapplication.data

import com.example.testapplication.data.db.album.Album
import com.example.testapplication.data.db.album.AlbumDao
import com.example.testapplication.data.web.Api

interface Repo {
    suspend fun receiveApiDataAsync(): List<Album>?

    suspend fun receiveDaoDataAsync(offset: Int, size: Int): List<Album>?

    suspend fun saveAll(albums: List<Album>)

}

class RepoImpl(private val api: Api, private val dao: AlbumDao) : Repo {
    override suspend fun receiveApiDataAsync() = api.getDataAsync()

    override suspend fun receiveDaoDataAsync(offset: Int, size: Int) = dao.getAlbums(offset, size)

    override suspend fun saveAll(albums: List<Album>) = dao.insert(albums)

}