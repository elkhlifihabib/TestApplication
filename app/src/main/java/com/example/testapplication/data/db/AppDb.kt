package com.example.testapplication.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.testapplication.data.db.album.Album
import com.example.testapplication.data.db.album.AlbumDao


@Database(
    exportSchema = true
    , version = 1
    , entities = [Album::class]
)

abstract class AppDb : RoomDatabase() {
    abstract fun albumDao(): AlbumDao

}