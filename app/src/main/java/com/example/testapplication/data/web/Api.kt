package com.example.testapplication.data.web

import com.example.testapplication.data.db.album.Album
import retrofit2.http.GET

interface Api {
    @GET("img/shared/technical-test.json")
    suspend fun getDataAsync(): List<Album>
}

object ApiConf {
    const val URL = "https://static.leboncoin.fr/"
}