package com.example.testapplication.data.db.album

import androidx.room.*

@Dao
interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(album: Album): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(albums: List<Album>)

    @Query("SELECT * FROM album  LIMIT :offset,:size")
    suspend fun getAlbums(offset: Int, size: Int): List<Album>?

    @Delete
    suspend fun delete(album: Album)

    @Query("SELECT * FROM album WHERE id = :id")
    suspend fun get(id: Long): Album?

}