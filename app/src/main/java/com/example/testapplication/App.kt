package com.example.testapplication

import android.app.Application
import com.example.testapplication.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

@Suppress("unused")
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        configureKoin()
    }

    private fun configureKoin() {
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(moduleConf)
        }
    }


}

val moduleConf = listOf(dbModule, daoModule, preferencesModule, remoteModule, viewModelModule)
