package com.example.testapplication.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

/**
 * ViewModel for Coroutines Jobs
 *
 * launch() - launch a Rx request
 * clear all request on stop
 */
abstract class CoroutineViewModel : ViewModel() {

    /**
     * This is the job for all coroutines started by this ViewModel.
     * Cancelling this job will cancel all coroutines started by this ViewModel.
     */
    private val viewModelJob = SupervisorJob()

    /**
     * This is the main scope for all coroutines launched by MainViewModel.
     * Since we pass viewModelJob, you can cancel all coroutines
     * launched by uiScope by calling viewModelJob.cancel()
     */
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     * Heavy operation that cannot be done in the Main Thread
     * Dispatchers.Main
     * optimized for disk | network | room | file
     */
    fun launchMain(code: suspend CoroutineScope.() -> Unit) {
        uiScope.launch(context = Dispatchers.Main, block = code)
    }

    /**
     * Dispatcher.IO
     * Heavy operation that cannot be done in the Main Thread
     * optimized for disk | network | room | file
     */

    /**
     * Dispatcher.Default
     * Heavy operation that cannot be done in the Main Thread
     * optimized for CPU intensive | sorting list | parsing json
     */

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}