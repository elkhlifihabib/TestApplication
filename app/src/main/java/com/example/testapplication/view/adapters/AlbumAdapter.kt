package com.example.testapplication.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.testapplication.R
import com.example.testapplication.data.db.album.Album

class AlbumAdapter(
    var list: List<Album>, private val clickListener: (Album) -> Unit
) : RecyclerView.Adapter<AlbumAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return ViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val albumd: Album = list[position]
        holder.bind(albumd, clickListener)
    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private var nameTextView: TextView? = null
        private var image: ImageView? = null

        init {
            nameTextView = itemView.findViewById(R.id.nameTextView)
            image = itemView.findViewById(R.id.imageView)
        }

        fun bind(album: Album, clickListener: (Album) -> Unit) {
            nameTextView?.text = album.title

            image!!.load(album.thumbnailUrl) { placeholder(R.drawable.empty_album) }
            itemView.setOnClickListener { clickListener(album) }
        }


    }

}

