package com.example.testapplication.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testapplication.R
import com.example.testapplication.data.db.album.Album
import com.example.testapplication.view.adapters.AlbumAdapter
import com.example.testapplication.viewModel.MainViewModel
import com.jcdecaux.livetouch.pdfreader.adapters.PaginationScrollListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_error.*
import kotlinx.android.synthetic.main.view_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModel()
    private var dataList: ArrayList<Album> = arrayListOf()
    private var layoutManager: LinearLayoutManager? = null
    private lateinit var adapter: AlbumAdapter

    private var isLastPage: Boolean = false
    private var isLoading: Boolean = false
    private var page: Int = 0
    private var size: Int = 10
    private var total: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        viewModel.command.observe(this, Observer { command ->
            command?.let {
                when (command) {
                    MainViewModel.Command.LoadingCommand -> showIsLoading()
                    is MainViewModel.Command.SuccessCommand -> showIsLoaded(command.await)
                    is MainViewModel.Command.FailedCommand -> showError(command.error)
                    else -> doNothing()
                }
            }
        })
        initViews()
    }

    private fun showIsLoading() {
        isLoading = true
        on_progress.visibility = View.VISIBLE
    }

    private fun showIsLoaded(await: List<Album>?) {
        dataList.addAll(await!!)
        adapter.notifyDataSetChanged()
        setPage()
        isLoading = false
        on_progress.visibility = View.INVISIBLE
    }


    private fun showError(error: Throwable) {
        isLoading = false
        on_progress.visibility = View.INVISIBLE
        if (total == 0) {
            on_error.visibility = View.VISIBLE
        }
        error.printStackTrace()
    }

    private fun doNothing() {
        on_progress.visibility = View.INVISIBLE
    }


    private fun albumItemClicked(albumd: Album) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("url", albumd.url)
        intent.putExtra("title", albumd.title)
        startActivity(intent)
    }

    private fun setRecyclerView() {
        adapter = AlbumAdapter(dataList) { albumd: Album -> albumItemClicked(albumd) }
        layoutManager = LinearLayoutManager(this)
        recycler_albums.layoutManager = layoutManager
        recycler_albums.setHasFixedSize(true)
        recycler_albums.adapter = adapter
        recycler_albums.addOnScrollListener(object : PaginationScrollListener(layoutManager!!) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                viewModel.getData(page, size)
                isLoading = true

            }

        })


    }

    private fun initViews() {
        total = viewModel.total
        setRecyclerView()
        viewModel.getData(page, size)

    }

    private fun setPage() {
        if (dataList.size < total) {
            page += 1
        }
        if (total - dataList.size <= size) {
            isLastPage = true
        }
    }
}
