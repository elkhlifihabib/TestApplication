package com.example.testapplication.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import coil.api.load
import com.example.testapplication.R
import kotlinx.android.synthetic.main.item.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val title = intent.getStringExtra("title")
        val url = intent.getStringExtra("url")
        initViews(title, url)
    }

    private fun initViews(title: String, url: String) {
        nameTextView?.text = title
        imageView.load(url) {
            placeholder(R.drawable.empty_album)
            error(R.drawable.empty_album)
        }
    }
}
