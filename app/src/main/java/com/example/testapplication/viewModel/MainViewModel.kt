package com.example.testapplication.viewModel


import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import com.example.testapplication.base.CoroutineViewModel
import com.example.testapplication.base.SingleLiveEvent
import com.example.testapplication.data.RepoImpl
import com.example.testapplication.data.db.album.Album
import com.example.testapplication.di.SHARED_TOTAL

class MainViewModel(
    private val repoImpl: RepoImpl,
    private val sharedPreferences: SharedPreferences
) : CoroutineViewModel() {

    var total: Int = sharedPreferences.getInt(SHARED_TOTAL, 0)

    sealed class Command {
        object LoadingCommand : Command()
        class SuccessCommand(val await: List<Album>) : Command()
        class FailedCommand(val error: Throwable) : Command()
    }

    private val mCommand = SingleLiveEvent<Command>()
    val command: LiveData<Command>
        get() = mCommand


    fun getData(offset: Int, size: Int) {
        mCommand.value = Command.LoadingCommand
        launchMain {
            try {
                val awaitDao = repoImpl.receiveDaoDataAsync(offset, size)
                if (!awaitDao.isNullOrEmpty()) {
                    mCommand.value = awaitDao.let { Command.SuccessCommand(it) }
                } else {
                    val awaitApi = repoImpl.receiveApiDataAsync()
                    repoImpl.saveAll(awaitApi)
                    with(sharedPreferences.edit()) {
                        putInt(SHARED_TOTAL, awaitApi.size)
                        commit()
                    }
                    val awaitDao = repoImpl.receiveDaoDataAsync(offset, size)
                    mCommand.value = Command.SuccessCommand(awaitDao!!)
                }

            } catch (error: Throwable) {
                mCommand.value = Command.FailedCommand(error)
            }
        }
    }
}